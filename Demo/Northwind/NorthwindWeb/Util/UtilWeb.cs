﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
namespace NorthwindWeb.Util
{
    static class UtilWeb
    {
        public static void Imprimir(String mensaje)
        {
            StreamWriter StreamWriter = new StreamWriter("D:\\Mensaje", true);
            StreamWriter.WriteLine("********** {0} **********", DateTime.Now);
            StreamWriter.Write("Mensaje: " + mensaje);
            StreamWriter.WriteLine();
            StreamWriter.Close();
        }

        public static void ValidarSesion()
        {

        }

        public static void LogException(Exception Exception, string source)
        {
            string RutaDelArchivo = System.Configuration.ConfigurationManager.AppSettings["RutaLog"].ToString();
            StreamWriter StreamWriter = new StreamWriter(RutaDelArchivo, true);
            StreamWriter.WriteLine("********** {0} **********", DateTime.Now);
            if (Exception.InnerException != null)
            {
                StreamWriter.Write("Inner Exception Type: ");
                StreamWriter.WriteLine(Exception.InnerException.GetType().ToString());
                StreamWriter.Write("Inner Exception: ");
                StreamWriter.WriteLine(Exception.InnerException.Message);
                StreamWriter.Write("Inner Source: ");
                StreamWriter.WriteLine(Exception.InnerException.Source);
                if (Exception.InnerException.StackTrace != null)
                {
                    StreamWriter.WriteLine("Inner Stack Trace: ");
                    StreamWriter.WriteLine(Exception.InnerException.StackTrace);
                }
            }
            StreamWriter.Write("Exception Type: ");
            StreamWriter.WriteLine(Exception.GetType().ToString());
            StreamWriter.WriteLine("Exception: " + Exception.Message);
            StreamWriter.WriteLine("Source: " + source);
            StreamWriter.WriteLine("Stack Trace: ");
            if (Exception.StackTrace != null)
            {
                StreamWriter.WriteLine(Exception.StackTrace);
                StreamWriter.WriteLine();
            }
            StreamWriter.Close();
        }

        
    }
}