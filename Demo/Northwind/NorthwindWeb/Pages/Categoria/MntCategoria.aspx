﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterInternal.Master" AutoEventWireup="true" CodeBehind="MntCategoria.aspx.cs" Inherits="NorthwindWeb.Pages.Categoria.MntCategoria" %>

<%@ Import Namespace="NorthwindBusiness.Business" %>
<%@ Import Namespace="NorthwindEntity.Entity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div id="divPrincipal">
        <div>
            <asp:GridView ID="GvCategorias" runat="server" AutoGenerateColumns="False" OnRowEditing="GvCategorias_RowEditing" OnRowDeleting="GvCategorias_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="CategoryID" HeaderText="Código" />
                    <asp:BoundField DataField="CategoryName" HeaderText="Nombre" />
                    <asp:CommandField CancelText="" ShowDeleteButton="True" ShowEditButton="True" DeleteImageUrl="~/Resources/img/delete.png" EditImageUrl="~/Resources/img/edit.png" ButtonType="Image" DeleteText="Eliminar " EditText="Editar " />
                </Columns>
            </asp:GridView>
        </div>
        <br />
        <div>
            <asp:Label ID="LblMensaje" runat="server" Text="Label"></asp:Label>
        </div>
    </div>
</asp:Content>
