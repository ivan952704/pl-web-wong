﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Importaciones
using NorthwindBusiness.Business;
using NorthwindEntity.Entity;
using NorthwindWeb.Util;

namespace NorthwindWeb.Pages.Categoria {
    public partial class MntCategoria : System.Web.UI.Page {

        private CategoriesBusiness CategoriesBusiness = CategoriesBusiness.ObtenerInstancia();

        protected void Page_Load(object sender, EventArgs e) {
            LlenarDagrid();
            this.LblMensaje.Text = "";
        }

        private void LlenarDagrid()
        {
            GvCategorias.DataSource = CategoriesBusiness.Listar();
            GvCategorias.DataBind();
        }


        protected void GvCategorias_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow row = GvCategorias.Rows[e.NewEditIndex];
            e.Cancel = true;
            Response.Redirect("/Pages/Categoria/ActCategoria.aspx?id=" + row.Cells[0].Text);
        }

        
        protected void GvCategorias_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                CategoriesBusiness.Eliminar(Convert.ToInt32(e.Values[0].ToString()));
                this.LblMensaje.Text = string.Format(ConstantesWeb.EXITO_ELIMINAR, "Categoria");
                this.LlenarDagrid();
            }
            catch (Exception Ex)
            {
                this.LblMensaje.Text = "Ocurrio un error: " + Ex.Message;
            }
            

        }
    
       
    }
}