﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NorthwindEntity.Entity;
using NorthwindWeb.Util;

namespace NorthwindWeb {
    public partial class Index : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (Request.QueryString["men"]!=null && Request.QueryString["men"].Equals("0"))
            {
                this.LblMensaje.Text = ConstantesWeb.USUARIO_NO_SESION;
            }
            else
            {
                this.LblMensaje.Text = "";
            }

            if (Request.Cookies["UsuarioCookie"] != null)
            {
                if (Request.Cookies["UsuarioCookie"]["Usuario"] != null)
                {
                    this.TxtUsuario.Text = Request.Cookies["UsuarioCookie"]["Usuario"]; 
                }

                if (Request.Cookies["UsuarioCookie"]["Clave"] != null)
                {
                    this.TxtUsuario.Text = Request.Cookies["UsuarioCookie"]["Clave"];
                }

                this.ChkGrabar.Checked = true;
            }
            else
            {
                this.ChkGrabar.Checked = false;
            }
        }

        protected void BtnIniciar_Click(object sender, EventArgs e) {
            
            if (TxtUsuario.Text.Equals("categoria") && TxtClave.Text.Equals("categoria"))
            {
                UsersDto UsersDto = new UsersDto();
                UsersDto.Name = "Henry Joe";
                UsersDto.LastName = "Wong Urquiza";
                Session.Clear();
                Session.Add(ConstantesWeb.SESION_USUARIO, UsersDto);
                if (this.ChkGrabar.Checked == true)
                {
                    HttpCookie UsuarioCookie = new HttpCookie("UsuarioCookie");
                    UsuarioCookie["Usuario"] = this.TxtUsuario.Text;
                    UsuarioCookie["Clave"] = this.TxtClave.Text;
                    UsuarioCookie.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(UsuarioCookie);
                }
                Response.Redirect("Pages/Categoria/MntCategoria.aspx");

            }
            
            else
            {
                LblMensaje.Text = ConstantesWeb.USUARIO_CLAVE_INVALIDA;
            }
            
        }
    }
}