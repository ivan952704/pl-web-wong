﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MasterExternal.Master" Inherits="System.Web.Mvc.ViewPage<NorthwindEntity.Entity.UsersDto>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm()) { %>
    <%: Html.AntiForgeryToken() %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        
        

        <div class="editor-label">
            Usuario:
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Name) %>
            <%: Html.ValidationMessageFor(model => model.Name) %>
        </div>

      

        <div class="editor-label">
            Clave:
        </div>
        <div class="editor-field">
            <%: Html.PasswordFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>

          <p>
            <input type="submit" value="Iniciar" />
        </p>
    </fieldset>
<% } %>


    <div>
    <%: ViewBag.Message %>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
