﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MasterInternal.Master" Inherits="System.Web.Mvc.ViewPage<NorthwindEntity.Entity.CategoriesDto>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm()) { %>
    <%: Html.AntiForgeryToken() %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>CategoriesDto</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CategoryID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CategoryID,  new {@readonly="true" })  %>
            <%: Html.ValidationMessageFor(model => model.CategoryID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CategoryName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CategoryName) %>
            <%: Html.ValidationMessageFor(model => model.CategoryName) %>
            
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Description) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Description) %>
            <%: Html.ValidationMessageFor(model => model.Description) %>
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: ViewBag.Message %>
</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
