﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MasterInternal.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<NorthwindEntity.Entity.CategoriesDto>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<table>
    <tr>
        <th>
            <%: Html.DisplayNameFor(model => model.CategoryID) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.CategoryName) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Description) %>
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.CategoryID) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.CategoryName) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Description) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "ActCategoria", new {  Id=item.CategoryID  }) %> |
            <%: Html.ActionLink("Eliminar", "EliminarCategoria", new { Id=item.CategoryID  }) %>
            <%: Html.ActionLink("Inicio", "Index","Principal") %>
            
        </td>
    </tr>
<% } %>

</table>

<div>
    <%: ViewBag.Message %>
</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
