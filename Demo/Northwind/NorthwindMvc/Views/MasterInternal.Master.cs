﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthwindMvc.Util;

namespace NorthwindMvc.Pages
{
    public partial class MasterInternal : System.Web.UI.MasterPage {
        protected void Page_Load(object sender, EventArgs e) {
            if (Session == null || Session[ConstantesWeb.SESION_USUARIO] == null)
            {
                Response.Redirect("~/Principal/");
            }

        }
    }
}