﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthwindMvc.Util
{
    public static class ConstantesWeb
    {

        public static readonly String NO_SELECCIONO_ELEMENTO = "Debe de presionar el botón de nuevo o seleccionar un elemento";
        public static readonly String EXITO_GUARDAR = "Se guardo correctamente el(la) {0}";
        public static readonly String EXITO_ACTUALIZAR = "Se actualizo correctamente el(la) {0}";
        public static readonly String EXITO_ELIMINAR = "Se elimino correctamente el(la) {0}";
        public static readonly String ERROR = "Ocurrio un error inesperado, contactar con el administrador : {0}";
        public static readonly String SESION_USUARIO = "UsuarioInicio";
        public static readonly String USUARIO_CLAVE_INVALIDA = "Usuario o clave invalida";
        public static readonly String USUARIO_NO_SESION = "El usuario no inicio sesión";
    }
}