﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//Importaciones
using NorthwindEntity.Entity;
using NorthwindBusiness.Business;
using NorthwindMvc.Util;

namespace NorthwindMvc.Controllers
{
    public class PrincipalController : Controller
    {
        //
        // GET: /Principal/

        public ActionResult Index()
        {
            ViewBag.Message = "";
            return View();
        }

        [HttpPost]
        public ActionResult Index(UsersDto UsersDto)
        {
            if (UsersDto.Name.Equals("categoria") && UsersDto.Password.Equals("categoria"))
            {
                UsersDto.Name = "Henry Joe";
                UsersDto.LastName = "Wong Urquiza";
                Session.Clear();
                Session.Add(ConstantesWeb.SESION_USUARIO, UsersDto);
                Response.Redirect("~/Categories/");
                return null;
            }
            else
            {
                ViewBag.Message = "Clave incorrecta";
            }
            return View();
        }

    }
}
