﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//Importaciones
using NorthwindEntity.Entity;
using NorthwindBusiness.Business;
using NorthwindMvc.Util;

namespace NorthwindMvc.Controllers
{
   
    public class CategoriesController : Controller
    {
        private CategoriesBusiness CategoriesBusiness = CategoriesBusiness.ObtenerInstancia();

        public ActionResult Index()
        {
            ViewBag.Message = "";
            return View(CategoriesBusiness.Listar());
        }


        public ActionResult RegCategoria()
        {
            ViewBag.Message = "";
            return View();
        }

        [HttpPost]
        public ActionResult RegCategoria(CategoriesDto CategoriesDto)
        {
            CategoriesBusiness.Insertar(CategoriesDto);
            ViewBag.Message = string.Format(ConstantesWeb.EXITO_GUARDAR, "Categoria");
            return View();
        }

        public ActionResult ActCategoria(int Id)
        {
            ViewBag.Message = "";
            return View(CategoriesBusiness.Obtener(Id));
        }

        [HttpPost]
        public ActionResult ActCategoria(CategoriesDto CategoriesDto)
        {
            CategoriesBusiness.Actualizar(CategoriesDto);
            ViewBag.Message = string.Format(ConstantesWeb.EXITO_ACTUALIZAR, "Categoria");
            return View();
        }
        
        public ActionResult EliminarCategoria(int Id)
        {
            CategoriesBusiness.Eliminar(Id);
            ViewBag.Message = string.Format(ConstantesWeb.EXITO_ELIMINAR, "Categoria");
            return View("Index", CategoriesBusiness.Listar());
        }
    }
}
